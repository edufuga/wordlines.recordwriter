package com.gitlab.edufuga.wordlines.recordwriter;

import com.gitlab.edufuga.wordlines.core.Status;
import com.gitlab.edufuga.wordlines.core.WordStatusDate;
import com.gitlab.edufuga.wordlines.recordreader.FileSystemRecordReader;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class RecordWriterService {
    private final Path statusRoot;
    private final Map<String, FileSystemRecordReader> readersByLanguage;
    private final Map<String, RecordWriter> writersByLanguage;

    public RecordWriterService() {
        this.readersByLanguage = new HashMap<>();
        this.writersByLanguage = new HashMap<>();

        String vocabularyRootAsString = System.getProperty("bookwards.vocabulary");
        if (vocabularyRootAsString == null || vocabularyRootAsString.isEmpty()) {
            Map<String, String> env = System.getenv();
            if (!env.containsKey("WORDSFILES")) {
                throw new RuntimeException("Neither the system property 'bookwards.vocabulary' " +
                        "nor the environment variable 'WORDSFILES' was set! You have to tell me where your vocabulary " +
                        "is found.");
            }
            vocabularyRootAsString = env.get("WORDSFILES");
        }
        System.out.println("Vocabulary root: " + vocabularyRootAsString);

        this.statusRoot = Paths.get(vocabularyRootAsString).resolve("status");
        System.out.println(statusRoot.toAbsolutePath());
        if (Files.notExists(statusRoot)) {
            throw new RuntimeException("Words files root not found");
        }
    }

    public WordStatusDate writeRecord(String language, String word, String status) throws Exception {
        if (language == null || language.isEmpty()) {
            return null;
        }
        language = language.toUpperCase();

        if (word == null || word.isEmpty()) {
            return null;
        }

        RecordWriter recordWriter = getRecordWriterFor(language);

        Status st = Status.valueOf(status);

        WordStatusDate record = null;

        try {
            record = new WordStatusDate(word, st, new Date());
            recordWriter.write(record);
        }
        catch (Exception ignored) {
        }

        return record;
    }

    private RecordWriter getRecordWriterFor(String language) throws Exception {
        Path statusFolder = statusRoot.resolve(language);
        FileSystemRecordReader reader = getRecordReaderFor(language);
        writersByLanguage.putIfAbsent(language, new RecordWriter(statusFolder.toString(), reader));
        return writersByLanguage.get(language);
    }

    private FileSystemRecordReader getRecordReaderFor(String language) throws Exception {
        Path statusFolder = statusRoot.resolve(language);
        readersByLanguage.putIfAbsent(language, new FileSystemRecordReader(statusFolder.toAbsolutePath().toString(), null));
        return readersByLanguage.get(language);
    }

    public static void main(String[] args) {
        System.out.println("The RecordWriterService is meant to be used as a dependency.");
    }
}
