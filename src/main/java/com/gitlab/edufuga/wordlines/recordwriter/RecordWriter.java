package com.gitlab.edufuga.wordlines.recordwriter;

import com.gitlab.edufuga.wordlines.core.WordStatusDate;
import com.gitlab.edufuga.wordlines.recordreader.FileSystemRecordReader;
import com.gitlab.edufuga.wordlines.recordreader.RecordReader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class RecordWriter {
    private final String statusFolder;
    private final FileSystemRecordReader recordReader;

    public RecordWriter(String statusFolder, FileSystemRecordReader recordReader) {
        this.statusFolder = statusFolder;
        this.recordReader = recordReader;
    }

    public void write(WordStatusDate result) throws IOException, ParseException {
        String filename = result.getWord();
        Path statusFolder = Paths.get(this.statusFolder);
        Path filePath = statusFolder.resolve(filename + ".tsv");
        if (Files.exists(filePath)) {
            // Previous file found. Moving to history folder.
            WordStatusDate previousResult = recordReader.readRecord(result.getWord());
            LocalDate localDate = convertToLocalDateViaInstant(previousResult.getDate());
            String previousResultDateString = localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            Path destinationFolder = statusFolder.resolve("history").resolve(previousResultDateString);
            Files.createDirectories(destinationFolder);
            Files.move(filePath, destinationFolder.resolve(filename + ".tsv"), REPLACE_EXISTING);
        }

        Files.write(filePath, (result.toString() + "\n").getBytes());

        recordReader.updateCache (result);
    }

    private static LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static void main(String[] args) {
        System.out.println ("This is the RecordWriter. I don't do anything yet.");
    }
}
